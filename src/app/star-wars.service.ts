import { LogService } from './log.service';
import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Character } from './character';


@Injectable()
export class StarWarsService{
    private characters = [
        {name : 'Luke Skywalker', side:'', ID:'0', Atrb:'positive'},
        {name : 'Darth Vader', side:'', ID:'0', Atrb:'positive'}
      ];

      private charactersURL = 'http://swapi.dev/api/people/';


      private logService: LogService;
      charactersChanged = new Subject<void>();
      http: HttpClient;

      constructor(logService: LogService, http: HttpClient){
        this.logService = logService;
        this.http = http;
      }

      getApiCharacters(): Observable<Character[]> {
        //We use HttpClient to get characters from the API
        return this.http.get<Character[]>(this.charactersURL)
          .pipe(
            //Star Wars API returns 
            //Used any to avoid warning over unserialized property
            map((characters:any) => characters.results)
          )
      }
      
      getCharacters( chosenList) {
        if(chosenList === 'all'){
          return this.characters.slice();
        }
        return this.characters.filter((char) => {
          return char.side === chosenList;
        })
      }

      getAllCharacters() {
        return [...this.characters];
      }

      getCharacter(id: number): Observable<Character> {
        //We use HttpClient to get characters from the API
        return this.http.get<Character>(this.charactersURL + id)
          .pipe(
            //Star Wars API returns 
            //Used any to avoid warning over unserialized property
            map((character:any) => character)
          )
      }

      OngetDetails (charInfo){
        const pos = this.characters.findIndex((char) => {
          return char.name === charInfo.name;
        }) 
        this.logService.writeLog('Character name' + charInfo.name);
      }

      
  onSideChosen(charInfo) {
    const pos = this.characters.findIndex((char) => {
      return char.name === charInfo.name;
    })
    this.characters[pos].side = charInfo.side;
    this.charactersChanged.next();
    this.logService.writeLog('Changed side of ' + charInfo.name + 'new side:' + charInfo.side);
      }

      addCharacter(name, side, ID, Atrb)
      {
        const pos = this.characters.findIndex((char)  => {
          return char.name === name;
        })
        if (pos !== -1) {
          return;
        }
        const newChar = {name: name, side: side, ID:ID, Atrb: Atrb};
        this.characters.push(newChar);
      }
    
}