import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { StarWarsService } from '../star-wars.service';
import { Character } from '../character';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
 
  characters: Character [];
  selectedCharacter: Character;
  activatedRoute: ActivatedRoute;
  swService: StarWarsService;
  subscription;

  constructor(activatedRoute: ActivatedRoute, swService: StarWarsService) {

    this.swService = swService;
    
  }
  getCharacters(): void {
    this.swService.getApiCharacters()
      .subscribe(characters => this.characters = characters);
  }

  

  ngOnInit() {
    this.getCharacters();
    
  }

  
}
