import { Component, OnInit, Input, Output } from '@angular/core';
import { StarWarsService } from '../star-wars.service';
import { ActivatedRoute, RouterModule, Router } from '@angular/router';
import { Character } from '../character';
import { HistoryService } from '../history.service';

@Component({
  selector: 'app-character-details',
  templateUrl: './character-details.component.html',
  styleUrls: ['./character-details.component.css']
})

export class CharacterDetailsComponent implements OnInit {

  @ Input() character: Character;

  
  constructor(
    private swService: StarWarsService, 
    private route: ActivatedRoute,
     private historyService: HistoryService) {
    this.swService = swService;
    this.route = route;
  }

ngOnInit(){
this.getCharacter();
if(this.route.snapshot.paramMap.get('id')){
  this.historyService.add('Character Details of Character ' + this.route.snapshot.paramMap.get('id'))
};
  
}

getCharacter(): void {
  const id = +this.route.snapshot.paramMap.get('id');
  this.swService.getCharacter(id)
    .subscribe(character => this.character = character);
}




 }
  

 
  
