import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TabsComponent } from './tabs/tabs.component';
import { ListComponent } from './list/list.component';
import { CreateCharacterComponent } from './create-character/create-character.component';
import { CharacterDetailsComponent } from './character-details/character-details.component';


const routes = [
  { path: 'characters', component: TabsComponent, children: [
  { path: '', redirectTo: 'all', pathMatch: 'full' },
  { path: ':side', component: ListComponent }]},
  { path: 'new-character', component:CreateCharacterComponent },
  { path:'character-details/:id', component:CharacterDetailsComponent},
  { path: '**', redirectTo: '/characters/all' }
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
