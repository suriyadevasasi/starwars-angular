import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { StarWarsService } from '../star-wars.service';
import { Character } from '../character';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  @Input() character;
  hidediv:boolean = true;

  characters: Character[]; 
  selectedCharacter: Character;

  
  data: string;
  swService: StarWarsService;

  

  constructor(swService: StarWarsService) {
    this.swService = swService;
  }

  ngOnInit() {
  }
  
  onSelect(character: Character): void{
    this.selectedCharacter = character;
    console.log("character length"+ character);
  }

  onclick(){
    this.hidediv = false;
  }
  
 
}
